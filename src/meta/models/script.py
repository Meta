from google.appengine.ext import db


class Script(db.Model):
  """
  """

  name = db.StringProperty(verbose_name="Script Name")

  content = db.StringProperty(verbose_name="Script content", multiline=True)
