from django import forms

from meta.logic.redirect import getEditRedirect
from meta.models import script
from meta import confs 


NAME = 'script'
MODEL = script.Script


confs.addConf({
    'name': NAME,
    'action': 'create',
    'method': 'GET',
    'template': "edit.html",
    'model': MODEL,
    'title': 'Create Script',
    'context': ['name', 'title'],
    })


confs.addConf({
    'name': NAME,
    'action': 'create',
    'method': 'POST',
    'template': "edit.html",
    'model': MODEL,
    'title': 'Create Script',
    'context': ['name', 'title'],
    })


confs.addConf({
    'name': NAME,
    'action': 'edit',
    'method': 'GET',
    'template': "edit.html",
    'model': MODEL,
    'title': 'Edit Script',
    'context': ['name', 'title'],
    })


confs.addConf({
    'name': NAME,
    'action': 'edit',
    'method': 'POST',
    'template': "edit.html",
    'model': MODEL,
    'title': 'Edit Script',
    'context': ['name', 'title'],
    })


confs.addConf({
    'name': NAME,
    'action': 'list',
    'method': 'GET',
    'template': "list.html",
    'model': MODEL,
    'title': 'List Script',
    'content': {
        'description': 'List of scripts',
        'row': 'list/row.html',
        'main': 'list/main.html',
        'heading': 'list/heading.html',
        'action': getEditRedirect
        },
    'context': ['name', 'title'],
    })
