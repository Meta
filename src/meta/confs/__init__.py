__confs = {}
__all__ = ['scripts']
aslist = list


class Error:
  def __init__(self, msg):
    self.msg = msg
  def __str__(self):
    return self.msg

def addConf(conf):
  """Adds a configuration for the specified type
  """

  global __confs

  if 'name' not in conf or 'action' not in conf or 'method' not in conf:
    raise Error("Conf if not a dictionary with a 'name', 'action' and 'method' field")

  name = conf['name']
  action = conf['action']
  method = conf['method']
  key = (name, action, method)

  if key in __confs:
    __confs.clear()
    raise Error("Cannot create two identical configurations")

  __confs[key] = conf

def getConf(name, action, method):
  key = (name, action, method)

  if key not in __confs:
    raise Error("Unknown type '%s:%s:%s'" % key)

  return __confs[key]

def list():
  res = set()
  for key, _, _ in __confs:
    res.add(key)

  return aslist(res)
