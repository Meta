from google.appengine.ext.db import djangoforms


class DynaFormMetaclass(djangoforms.ModelFormMetaclass):
  def __new__(cls, class_name, bases, attrs):
    meta = attrs.get('Meta')
    if meta:
      conf = getattr(meta, 'dynaconf', None)
      delattr(meta, 'dynaconf')

    if conf:
      meta.model = conf['model']
      for key, value in conf.get('extra', {}).iteritems():
        attrs[key] = value

    return super(DynaFormMetaclass, cls).__new__(cls, class_name, bases, attrs)


def DynaForm(conf):
  class DynaForm(djangoforms.ModelForm):
    __metaclass__ = DynaFormMetaclass
    class Meta:
      dynaconf = conf
  return DynaForm
