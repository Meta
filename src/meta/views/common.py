from meta.views.decorators import configured


def dispatch(display, catch):
  def wrapped(request, *args, **kwargs):
    """Dispatches a django request
    """
  
    if request.method == 'GET':
      return display(request=request, *args, **kwargs)
    else:
      return catch(request=request, *args, **kwargs)

  return wrapped

def catch(display):
  @configured
  def wrapped(request, conf):
    return display(request, conf=conf)

  return wrapped

