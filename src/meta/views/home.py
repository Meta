from django import http
from django.template import loader

from meta.logic.context import getContext


def dispatch(request):
  """
  """

  template = "home.html"
  context = getContext()

  content = loader.render_to_string(template, dictionary=context)
  return http.HttpResponse(content=content)
