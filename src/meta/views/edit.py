import logging

from google.appengine.ext import db

from django import http
from django.template import loader

from meta.forms import dynaform
from meta.models import script
from meta.logic.context import getContext
from meta.views import common
from meta.views.decorators import configured


@configured
def display(request, conf, name, form=None):
  """Displays an edit page, optionally using form
  """

  if name:
    logging.info("display: got name")
    model = conf['model']

    key = db.Key.from_path(model.kind(), int(name))
    item = model.get(key)
    form_class = dynaform.DynaForm(conf)
    form = form_class(instance=item)

  if not form:
    logging.info("display: No form")
    form_class = dynaform.DynaForm(conf)
    form = form_class()

  template = conf['template']
  context = getContext(conf)
  context['form'] = form
  context['title'] = conf['title']

  content = loader.render_to_string(template, dictionary=context)
  return http.HttpResponse(content=content)

@configured
def catch(request, conf, name):
  """Processes a POST request
  """

  model = conf['model']
  key = db.Key.from_path(model.kind(), int(name))
  item = model.get(key)
  form_class = dynaform.DynaForm(conf)
  form = form_class(request.POST, instance=item)

  if not form.is_valid():
    logging.info("catch: Form not valid")
    errs = form.errors
    raise "Wtf?"

  entity = form.save(True)
  logging.info("catch: Form saved")
  return display(request, form=form, conf=conf, name=name)


dispatch = common.dispatch(display, catch)
