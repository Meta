import logging

from django import http
from django.template import loader

from meta.logic.lists import Lists
from meta.logic.context import getContext
from meta.views import common
from meta.views.decorators import configured


@configured
def display(request, conf):
  """Displays an edit page, optionally using form
  """

  template = conf['template']
  model = conf['model']

  entities = model.all().fetch(1000)

  content = conf['content'].copy()
  content['data'] = entities
  content['conf'] = conf

  contents = [content]

  context = getContext(conf)
  context['list'] = Lists(contents)

  content = loader.render_to_string(template, dictionary=context)
  return http.HttpResponse(content=content)


catch = common.catch(display)
dispatch = common.dispatch(display, catch)
