import logging

from django import http
from django.template import loader

from meta.logic.context import getContext
from meta.forms import dynaform
from meta.views import common
from meta.views.decorators import configured


@configured
def display(request, conf, form=None, name=None):
  if not form:
    logging.info("display: No form")
    form_class = dynaform.DynaForm(conf)
    form = form_class()

  template = conf['template']
  context = getContext(conf)
  context['form'] = form

  content = loader.render_to_string(template, dictionary=context)
  return http.HttpResponse(content=content)


@configured
def catch(request, conf):
  """Processes a POST request
  """

  form_class = dynaform.DynaForm(conf)
  form = form_class(request.POST)

  if not form.is_valid():
    logging.info("catch: Form not valid")
    errs = form.errors
    raise "Wtf?"

  entity = form.save(True)
  logging.info("catch: Form saved")
  return http.HttpResponseRedirect('/edit/%s/%s' % (conf['name'], entity.key().id()))


dispatch = common.dispatch(display, catch)
