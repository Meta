from functools import wraps

from meta import confs
from meta.confs import *


def configured(func):
  @wraps(func)
  def wrapper(*args, **kwargs):
    if 'conf' not in kwargs:
      id = kwargs.pop('confid')
      act = kwargs.pop('confact')
      request = kwargs['request']
      conf = confs.getConf(id, act, request.method)
    else:
      conf = kwargs.pop('conf')

    return func(conf=conf, *args, **kwargs)

  return wrapper
