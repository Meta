from meta import confs
from meta.confs import *


def getContextForConf(conf, context):
  for field in conf.get('context', []):
    context[field] = conf.get(field)

  return context

def getContext(conf=None):
  context = {}
  context['confs'] = confs.list()

  if conf:
    context = getContextForConf(conf, context)

  return context
