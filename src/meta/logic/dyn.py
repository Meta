import sys
import os


def dynload(filename, content):
  """Dynamically load a file
  """

  file = open(filename, "w")

  # Write all args to file
  for line in content:
    file.write(line)
    file.write("\n")

  file.close()

  module = __import__("test", fromlist=True)
  os.remove(filename)
  os.remove("%sc" % filename)

  return module

def dyncall(module, method, args, kwargs):
  func = getattr(module, method)
  func(*args, **kwargs)

