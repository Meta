class Lists(object):
  """List array suitable for enumerating over with just 'for in'
  """

  def __init__(self, contents):
    """Constructs a new Lists object with the specified contents
    """

    # All the contents of all the lists
    self.contents = contents
    self.content = {}

    # For iterating over all the lists
    self.lists = range(len(contents))
    self.list_data = []

    # For iterating over the rows
    self.rows = []
    self.row_data = []

  def get(self, item):
    """Returns the item for the current list data
    """

    return self.content[item]

  def next_list(self):
    """Shifts out the current list content

    The main content of the next list is returned for further processing.
    """

    # Advance the list data once
    self.content = self.contents[0]
    self.contents = self.contents[1:]

    # Update internal 'iterators'
    self.list_data = self.get('data')
    self.rows = range(len(self.list_data))

    return self.get('main')

  def next_row(self):
    """Returns the next list row for the current list

    Before calling this method, next_list should be called at least once.
    """

    # Update internal 'iterators'
    self.row_data =  self.list_data[0]

    # Advance the row data once
    self.list_data = self.list_data[1:]

    return self.get('row')

  def lists(self):
    """Returns a list of numbers the size of the amount of lists.

    This method can be used to iterate over all lists with shift,
    without using a while loop.
    """

    return self.lists

  def rows(self):
    """Returns a list of numbers the size of the amount of items.

    This method can be used to iterate over all items with next for
    the current list, without using a while loop.
    """

    return self.rows

  def item(self):
    """Returns the current row item for the current list

    Before calling this method, next_row should be called at least once.
    """

    return self.row_data

  def redirect(self):
    """Returns the redirect for the current row item in the current list.
    """

    conf = self.get('conf')
    action = self.get('action')
    result = action(self.row_data, conf)
    return result

  def description(self):
    return self.get('description')

  def heading(self):
    return self.get('heading')

  def row(self):
    return self.get('row')
