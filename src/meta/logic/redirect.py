def getEditRedirect(entity, conf):
  return '/edit/%s/%s' % (conf['name'], entity.key().id())
