from django.conf.urls.defaults import patterns

core = r'[0-9a-zA-Z](?:[0-9a-zA-Z]|_[0-9a-zA-Z])*'
id_patrn = r'(?P<confid>%s)' % core
name_patrn = r'(?P<name>%s)' % core
act_patrn = r'(?P<confact>%s)'
create_patrn = act_patrn % "create"
edit_patrn = act_patrn % "edit"
list_patrn = act_patrn % "list"

urlpatterns = patterns('',
    ('^$', 'meta.views.home.dispatch'),
    ('^%s/%s$' % (create_patrn, id_patrn), 'meta.views.create.dispatch'),
    ('^%s/%s/%s$' % (edit_patrn, id_patrn, name_patrn), 'meta.views.edit.dispatch'),
    ('^%s/%s$' % (list_patrn, id_patrn), 'meta.views.list.dispatch'),
)
